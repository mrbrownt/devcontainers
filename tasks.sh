#!/bin/sh

# Exit on err return
set -e
# Exit on err return in pipe
set -o
# Trace bash
[ -n "$TRACE" ] && set -x

if [ -z "$1" ]; then
    echo "This script needs a target to build"
fi

if [ -n "${GITLAB_CI}" ]; then
    docker login \
        -p "${CI_REGISTRY_PASSWORD}" \
        -u "${CI_REGISTRY_USER}" \
        "${CI_REGISTRY}"
fi

build() {
    DOCKER_IMG_NAME=$1

    cd "$DOCKER_IMG_NAME"
    docker build -t "registry.gitlab.com/mrbrownt/devcontainers/$DOCKER_IMG_NAME:latest" .
}

push() {
    DOCKER_IMG_NAME=$1

    docker push "registry.gitlab.com/mrbrownt/devcontainers/$DOCKER_IMG_NAME:latest"
}

case ${1} in
    build)
        build "${2}"
        ;;
    push)
        push "${2}"
        ;;
    *)
        echo "pipeline task was not specified"
        exit 1
        ;;
esac
